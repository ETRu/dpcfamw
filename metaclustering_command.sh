
#!/bin/bash

# Author: Elena Tea Russo @ Institute for Research and Technologies (Area Science Park) & SISSA. 
# Package: DPCfamW version 1.0
# Description: wrapper for metaclustering code

# SPDX-License-Identifier: CC-BY-4.0


export LC_NUMERIC="en_US.UTF-8"

# folders ALWAYS without final /
dmat_infolder_wp=$1
output_folder_wp=$2
Nblocks=$3    # dummy variable, not really needed in this case. Still, let's maintain the standard used do far...

# check if Nblocks variable is reasonable
if [ -z $Nblocks ]
then
 echo "\$Nblocks is empty! ABORTING. Check your configure file!"
 exit 1
fi

[ -n "$Nblocks" ] && [ "$Nblocks" -eq "$Nblocks" ] 2>/dev/null
if [ $? -ne 0 ]; then
   echo "$Nblocks is not an integer number! Check your configure file!"
   exit 1
fi

if [ 1 -gt $Nblocks ]; then
 echo "$Nblocks is not a valid number of blocks! Check your configure file!"
 exit 1
fi


# run command
time bin/metaclustering.x ${dmat_infolder_wp}/*.bin -o ${output_folder_wp}/MCoutput.txt

# generate MClabels.txt
awk  '{print $4}'  ${output_folder_wp}/MCoutput.txt > ${output_folder_wp}/MClabels.txt

# move other output
mv peaksIdx.txt ${output_folder_wp}/

# clean other data
rm distII.txt
rm labels_before_merge.txt   

 