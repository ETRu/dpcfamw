#!/bin/bash

# Author: Elena Tea Russo @ Institute for Research and Technologies (Area Science Park) & SISSA. 
# Package: DPCfamW version 1.0
# Description: wrapper for traceback code

# SPDX-License-Identifier: CC-BY-4.0

export LC_NUMERIC="en_US.UTF-8"

# folders ALWAYS without final /
BINinput_folder_wp=$1  #  folder where binaries of primary clusters are stored (primary_clusters folder)
MCinput_folder_wp=$2 # folder where MClabels.txt is stored (metaclusters folder)
MCoutput_folder_wp=$3 # output folder (technically same as above but it can be different)
Nblocks=$4    #not really needed in this case, we use it to decide how many output to print, but it has not to be consistent actually. Still, we do this.

# check if Nblocks variable is reasonable
if [ -z $Nblocks ]
then
 echo "\$Nblocks is empty! ABORTING. Check your configure file!"
 exit 1
fi

[ -n "$Nblocks" ] && [ "$Nblocks" -eq "$Nblocks" ] 2>/dev/null
if [ $? -ne 0 ]; then
   echo "$Nblocks is not an integer number! Check your configure file!"
   exit 1
fi

if [ 1 -gt $Nblocks ]; then
 echo "$Nblocks is not a valid number of blocks! Check your configure file!"
 exit 1
fi


# run command
time bin/traceback.x ${BINinput_folder_wp}/*.bin  -l ${MCinput_folder_wp}/MClabels.txt  -o ${MCoutput_folder_wp}/

