# DPCfamW 1.0

DPCfam Workstation version.
Runs on Linux-based systems. Developed and tested on Ubuntu 18.
DPCfamW uses the moodycamel::ConcurrentQueue library ( https://github.com/cameron314/concurrentqueue )  freely available provided citation (Simplified BSD license). 

This version replicates the pipeline used in to anlayze UniRef50 (v. 2017_07) as in *Unsupervised protein family classification by Density Peak clustering, Russo ET, 2020, PhD Thesis* ( http://hdl.handle.net/20.500.11767/116345 ), but with smaller datasets.
Largest dataset we analysed is the TESTproteins_cd50.fasta datased we provide in this package. Due to memory bounds we do not guarantee that the abalysis of largest datasets is acheivable with this version.

## Requirements
- blastp (ncbi-blast-2.2.30+), installed and linked

Some standard bash programs are required:
- g++
- make
- sort
- shuf 
- awk

## Installation
Enter the package root directory and run
`make`

## Usage
### Configuration
In the package root directory:
1) Open file **config**.
The second line of this file contains the number of blocks in which the input data will be divided. Standard number of blocks is **5**. You can change it at youw will. Nothe that a number larger than half the number of sequeces to anlyse will not be admitted.

2) Open file **run_all.sh**.
Put your fasta data in a given folder.
Folder and filename must be stated at the beginning of run_all.sh as:

`datafolder="path_to_your_folder"`

`input_fasta_file="yourdata.fasta"`

Do *not* put the final slash (/) at the end of paths!


### Run DPCfamW
In the package root directory, run:

`./run_all.sh`

This will run the full pipeline.

### Global testing
In the package root directory, run:

`./run_all.sh test`

This will run the full pipeline using a small test dataset. The operation should take few minutes. At the end a series of ouputs will be checked with reference data.

## Example data
We provide as example data the dataset **TESTproteins_cd50.fasta**, located in the folder `data_example`.
This file includes about 5,000 protein sequences containing 3 different Pfam families, reduced at 50 P.I. using cd-hit.

### Estimated runtime of the pipeline using example data: 
On Intel i7, 8th gen and using *5* blocks:
- **Step 1**: alignment generation with blast - **75 minutes**
- **Step 2**: primary clustering - **10 minutes**
- **Step 3**: computation of distance matrix between primary clusters - **5 minutes**
- **Step 4**: metaclustering + merging - **<1 minute**
- **Last steps**: traceback, filtering, generation of MCS - **<1 minute**
 

### Description of proteins in TESTproteins_cd50.fasta

Proteins in this set contain either a PFAM Lon_C family (PF05362), a Lon_substr_bdg family (PF02190) or a  Pyrophosphatase family (PF00719).

Poteins have been selected by donwloading Pfam's alignments of each family above (Uniprot alignment).
We then downloaded each protein listed in the alignment from the UniprotKB website, specifically:
- 62066 proteins containing a PF05362 family region
- 51692 proteins containing a PF02190 family region
- 27691 proteins containing a PF00719 family region

We then run cd-hit to reduce reduindancy at 50 P.I.
**TESTproteins_cd50.fasta** contains **5,077** protein sequences
Note that PF05362 and PF02190 appear often in the same architecture.
Proteins may contain many other families.
*Downloaded on the 16th of march 2021*
*Pfam v 33.1*
*UniproKB v 2021_01*

# Authors
Algorithm and code by Elena Tea Russo & Federico Barone under the supervision of Alessandro Laio (SISSA, Trieste, IT), Marco Punta (HSR, Milan, IT) and Stefano Cozzini (AREA SCIENCE PARK, Trieste, IT).

Developed in SISSA, Trieste, IT (2016-2020) and AREA SCIENCE PARK, Trieste, IT (2021-)
