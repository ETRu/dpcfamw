#!/bin/bash

# Author: Elena Tea Russo @ Institute for Research and Technologies (Area Science Park) & SISSA. 
# Package: DPCfamW version 1.0
# Description:  bash pipeline to run all steps of DPCfamW

# SPDX-License-Identifier: CC-BY-4.0

export LC_NUMERIC="en_US.UTF-8"
config_file=configure
arg1=$1
# testing case

if [[ "$arg1" == test ]]; then
	echo "Global testing"
fi

###################################################
## Some specifics about the data you are woriking on 

## 1) absolute path of the folder containing data (DO *NOT* PUT A SLASH A THE END OF THE PATH!)
datafolder="/root/Work/testDPCWdata"
#datafolder="/root/Work/dpcfamw/globaltest"

## 2) input file name, containing fastas to be analised (!!!) to be located in the datafolder!
input_fasta_file="TESTproteins_cd50.fasta"
#cp ${datafolder}/data/proteins.fasta ${datafolder}/
#input_fasta_file="proteins.fasta"

## Set TEST environment
if [[ "$arg1" == test ]]; then
	echo "Global Testing; setting environment"
	datafolder="globaltest"
	echo "Setting datafolder as" $datafolder
	input_fasta_file="proteins.fasta"
	cp ${datafolder}/data/${input_fasta_file} ${datafolder}/
	echo "Setting input_fasta_file as" $input_fasta_file
	config_file="globaltest/data/configure"
fi


###################################################
## Build folder environment (generates folders inside datafolder)

fastas_folder_name="fastas"
fastas_folder=${datafolder}"/"${fastas_folder_name}
mkdir ${fastas_folder}

alignment_folder_name="alignments"
alignment_folder=${datafolder}"/"${alignment_folder_name}
mkdir ${alignment_folder}

primary_clusters_folder_name="primary_clusters"
primary_clusters_folder=${datafolder}"/"${primary_clusters_folder_name}
mkdir ${primary_clusters_folder}

dmat_folder_name="distance_matrix"
dmat_folder=${datafolder}"/"${dmat_folder_name}
mkdir ${dmat_folder}

MCs_folder_name="metaclusters"
MCs_folder=${datafolder}"/"${MCs_folder_name}
mkdir ${MCs_folder}

##  ... TODO: add other folders if/where needed




###################################################
## Read Nblocks from configure file
Nblocks=`awk 'NR==2{print $1}' ${config_file}`

# check if it is not empty
if [ -z $Nblocks ]
then
 echo "\$Nblocks is empty! ABORTING. Check your configure file!"
 exit 1
fi

# check if it is not <1
if [ 1 -gt $Nblocks ]; then
 echo "$Nblocks is not a valid number of blocks! Check your configure file!"
 exit 1
fi

# check if it is an integer number
[ -n "$Nblocks" ] && [ "$Nblocks" -eq "$Nblocks" ] 2>/dev/null
if [ $? -ne 0 ]; then
   echo "$Nblocks is not an integer number! Check your configure file!"
   exit 1
fi

echo "Working with $Nblocks blocks"

###################################################
## Prepare input data

echo ">>> Step 0: prepare data"

dict_file="dictionary.txt"
numeric_in_fasta_file=${input_fasta_file}".numid"

./prepare_dataset.sh ${datafolder}"/"${input_fasta_file} ${datafolder}"/"${dict_file} ${datafolder}"/"${numeric_in_fasta_file} ${fastas_folder} $Nblocks



###################################################
## MAIN Step 1: run all-vs-all local pairwise alignment generation
## This runs blast-command.sh, and it will generate the blast db, and then run the blast alignments.
echo ">>> Step 1: blast alignments"

./blast-command.sh ${datafolder} ${datafolder}"/"${numeric_in_fasta_file} ${fastas_folder} ${alignment_folder} $Nblocks 



###################################################
## MAIN Step 2: run primary clustering
## This compiles 200primary.cc and runs it
echo ">>> Step 2: primary clustering"

./primarycl-command.sh ${alignment_folder} ${primary_clusters_folder} $Nblocks


###################################################
## Step 3.1: fix output file with only the columns needed to be transformed into binary, also remove non-clustered alignments
## Columns: query_ID cluster_ID search_ID search_START_position search_END_position
## (!) cluster_ID is not absolute id, it is query-specific
## Note that the file is sorted w.r.t. query_ID
echo "> Step 3.1: clean primary output file"

./clean-output-primary.sh ${primary_clusters_folder} ${primary_clusters_folder} $Nblocks




###################################################
## Step 3.2: Transform all blocks to Binary
echo "> Step 3.2: convert to binary"

./generate_binary.sh ${primary_clusters_folder}  ${primary_clusters_folder} $Nblocks



###################################################
## MAIN Step 3.4 generate distance matrix between primary clusters<, then perform METACLUSTERINGecho ">>> Step 3.4: generate distance matrix"
echo ">>> Step 3.4: generate distance matrix"

./compute_dmat.sh ${primary_clusters_folder} ${dmat_folder} $Nblocks 

###################################################
## MAIN Step 3.5 METACLUSTERING + MERGING
echo ">>> Step 3.5: METACLUSTERING and MERGING"

./metaclustering_command.sh   ${dmat_folder}  ${MCs_folder} $Nblocks

###################################################
## MAIN Step 4: traceback
echo ">>> Step 4: traceback"

./traceback_command.sh  ${primary_clusters_folder} ${MCs_folder} ${MCs_folder} $Nblocks

###################################################
## MAIN Step 5: filtering

echo ">>> Step 5: filtering"
./filtering_command.sh ${MCs_folder} ${MCs_folder} $Nblocks


###################################################
## MAIN Step 6: GENERATE FINAL METACLUSTERS SEEDS (NON ALIGNED) with data (size, avglen, avgstd)
echo ">>> Step 6: GENERATE FINAL METACLUSTERS SEEDS"
#TODO    ${datafolder}"/"${numeric_in_fasta_file}.tab 

 ./create_final_MCs.sh ${MCs_folder} ${MCs_folder} ${datafolder}"/"${numeric_in_fasta_file}.tab  ${datafolder}"/"${dict_file}


###################################################
## GLOBAL TEST: CHECK OUTPUTS

if [[ "$arg1" == test ]]; then
	echo "Global Testing, CHECKING OUTPUTS"
	
	# outputs list:

	EC=0

	o_list="${dict_file} ${numeric_in_fasta_file} blastdb.phr  blastdb.pog  blastdb.psd  blastdb.psi  blastdb.psq"
	
	# check outputs
	for o in $o_list; do
	 #echo "checking file" $o
	 #echo "comparing files " ${datafolder}"/"$o " and " ${datafolder}"/"data/$o
	 diff ${datafolder}"/"$o ${datafolder}"/"data/$o &> tt
	 #diff ${datafolder}"/"$o ${datafolder}"/"data/$o 2>&1
	 check=`wc tt | awk '{print $3}'`
	 if [[ "$check" == 0 ]]; then
		echo '(v) test: ' $o 'ok' 
		else
		echo '(X) test: ERROR on' $o
	fi
	rm ${datafolder}"/"$o
	done


	# check iterativeley "Blocks" fastas outputs (fastas/i.fasta)
	for ((i=1;i<=Nblocks;++i)); do
	 diff ${fastas_folder}/${i}.fasta ${datafolder}/data/${fastas_folder_name}/${i}.fasta &> tt
	 check=`wc tt | awk '{print $3}'`
	 if (( check == 0 )); then
		echo '(v) test: ' ${i}.fasta 'ok'
		else
		echo '(X) test: ERROR on' ${i}.fasta
		EC=$((EC + 1))
	 fi
	rm tt
	rm ${fastas_folder}/${i}.fasta
	done

	# check iterativeley "Blocks" blast outputs (i.blasted)
	for ((i=1;i<=Nblocks;++i)); do
	 diff ${alignment_folder}/${i}.blasted ${datafolder}/data/${alignment_folder_name}/${i}.blasted &> tt
	 check=`wc tt | awk '{print $3}'`
	 if (( check == 0 )); then
		echo '(v) test: ' ${i}.blasted 'ok' 
		else
		echo '(X) test: ERROR on' ${i}.blasted 
		EC=$((EC + 1))
	 fi
	rm tt
	rm ${alignment_folder}/${i}.blasted
	done

	# check iterativeley "Blocks" primary clustering outputs (i.cl)
	for ((i=1;i<=Nblocks;++i)); do
	 diff ${primary_clusters_folder}/${i}.cl ${datafolder}/data/${primary_clusters_folder_name}/${i}.cl &> tt
	 check=`wc tt | awk '{print $3}'`
	 if (( check == 0 )); then
		echo '(v) test: ' ${i}.cl 'ok' 
		else
		echo '(X) test: ERROR on' ${i}.cl 
		EC=$((EC + 1))
	 fi
	rm tt
	rm ${primary_clusters_folder}/${i}.cl
	done


	# check iterativeley "Blocks" clean outputs (Ci.cl)
	for ((i=1;i<=Nblocks;++i)); do
	 diff ${primary_clusters_folder}/C${i}.cl ${datafolder}/data/${primary_clusters_folder_name}/C${i}.cl &> tt
	 check=`wc tt | awk '{print $3}'`
	 if (( check == 0 )); then
		echo '(v) test: ' C${i}.cl 'ok' 
		else
		echo '(X) test: ERROR on' C${i}.cl
		EC=$((EC + 1))
	 fi
	rm tt
	rm ${primary_clusters_folder}/C${i}.cl
	done


	# check iterativeley "Blocks" clean outputs (i.bin)
	for ((i=1;i<=Nblocks;++i)); do
	 diff ${primary_clusters_folder}/${i}.bin ${datafolder}/data/${primary_clusters_folder_name}/${i}.bin &> tt
	 check=`wc tt | awk '{print $3}'`
	 if (( check == 0 )); then
		echo '(v) test: ' ${i}.bin 'ok'
		else
		echo '(X) test: ERROR on' ${i}.bin
		EC=$((EC + 1))
	 fi
	rm tt
	rm ${primary_clusters_folder}/${i}.bin
	done

	# check iterativeley "Blocks" MATRIX outputs (i-j.bin)
	for ((i=1;i<=Nblocks;++i)); do
	for ((j=i;j<=Nblocks;++j)); do
	 diff  ${dmat_folder}/${i}-${j}.bin ${datafolder}/data/${dmat_folder_name}/${i}-${j}.bin&> tt
	 check=`wc tt | awk '{print $3}'`
	 if (( check == 0 )); then
		echo '(v) test: ' ${i}-${j}.bin 'ok' 
		else
		echo '(X) test: ERROR on' ${i}-${j}.bin 
		EC=$((EC + 1))
	 fi
	rm tt
	rm ${dmat_folder}/${i}-${j}.bin
	done
	done


	# check metaclustering, traceback, filtering and some final outputs
	cd ${MCs_folder}
	o_list=`ls`
	cd -

	# check outputs in o_list
	for o in $o_list; do
	diff ${MCs_folder}/$o  ${datafolder}/data/${MCs_folder_name}/$o &> tt
	check=`wc tt | awk '{print $3}'`
	 if (( check == 0 )); then
		echo '(v) test: ' $o 'ok'
		else
		echo '(X) test: ERROR on' $o 
		EC=$((EC + 1))
	 fi
	rm tt
	rm ${MCs_folder}/$o
	done
	
	
	# check MC seeds outputs
	cd ${MCs_folder}/seeds
	o_list=`ls`
	cd -

	# check outputs in o_list
	for o in $o_list; do
	diff ${MCs_folder}/seeds/$o  ${datafolder}/data/${MCs_folder_name}/seeds/$o &> tt
	check=`wc tt | awk '{print $3}'`
	 if (( check == 0 )); then
		echo '(v) test: ' $o 'ok'
		else
		echo '(X) test: ERROR on' $o 
		EC=$((EC + 1))
	 fi
	rm tt
	rm ${MCs_folder}/seeds/$o
	done

	echo "---------- GLOBAL TEST DONE"
	echo "NUMBER OF ERRORS (EC): " $EC

	rm -r ${fastas_folder}
	rm -r ${alignment_folder}
	rm -r ${primary_clusters_folder}
	rm -r ${dmat_folder}
	rm -r ${MCs_folder}
	rm ${datafolder}/proteins.fasta.numid.tab
	rm ${datafolder}/proteins.fasta
	rm ${datafolder}/blastdb.pin 

fi