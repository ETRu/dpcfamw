#!/bin/bash

# Author: Elena Tea Russo @ Institute for Research and Technologies (Area Science Park) & SISSA. 
# Package: DPCfamW version 1.0
# Description: unittest of ./generate_binary.sh 
# USAGE: ./test_generate_binary.sh  logfile outfile dataset
# where dataset can be either A B C or D 

# SPDX-License-Identifier: CC-BY-4.0

export LC_NUMERIC="en_US.UTF-8"

logfile=$1
outfile=$2
dataset=$3


date >> $logfile

Nblocks=`awk 'NR==2{print $1}' data/set${dataset}/configure `


# run command
cd ..
mkdir tests/primary_clusters
./generate_binary.sh   tests/data/set${dataset}/primary_clusters tests/primary_clusters $Nblocks >> tests/$logfile
cd -



EC=0
# check iterativeley "Blocks" outputs (i.bin)
for ((i=1;i<=Nblocks;++i)); do
 diff primary_clusters/${i}.bin data/set${dataset}/primary_clusters/${i}.bin &> tt
 check=`wc tt | awk '{print $3}'`
 if (( check == 0 )); then
	echo '(v) test: ' ${i}.bin 'ok' >> $logfile
	else
	echo '(X) test: ERROR on' ${i}.bin >> $logfile
	EC=$((EC + 1))
 fi
rm tt
rm primary_clusters/${i}.bin
done

echo "EC:" $EC >> $logfile
echo $EC > $outfile


rm -r primary_clusters


#if (($EC>0)); then
# exit $EC
#fi 
