// Author: Elena Tea Russo @ Institute for Research and Technologies (Area Science Park) & SISSA.
// Package: DPCfamW version 1.0
// Description: c++ program to check unittests results, used for GitLab CI/CD

// SPDX-License-Identifier: CC-BY-4.0


#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <cstdlib>
#include <iomanip>



using namespace std;




//---------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------
/////////////////////////////////   M   A   I   N    //////////////////////////////////////////////////////
//---------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------


// USAGE:  ./a.out  input_FOLDER  output_FOLDER dpar gpar START_fileID
// input is a BLAST output obtained from:
//	blastp  -query $myquery -db $mydb -evalue 0.1 -max_target_seqs 5000000 -out $myout -outfmt "6 qseqid sseqid qstart qend sstart send qlen slen length pident evalue score"

// !!! THIS VERSION HAS BEEN DEVELOPED TO MANAGE BLOCKS OF BLAST RESULTS, CONTAINING (ABOUT) 200 QUERIES


int main(int argc, char** argv) {

	ifstream infile; // input s;
	int number=0;

    // filenames
	string infile_path;
	stringstream sstm;
	// input file
	sstm << argv[1];
	infile_path = sstm.str();
	cout << "Checking File:        " << infile_path << endl;

	infile.open (infile_path.c_str());
	if (!infile.is_open()) { cout << "INFILE" << infile_path << " NOT GOOD" << endl; return 1; } 
 	
 
    // ++++++++++++++++++++++++++++++++++ READ FROM FILE +++++++++++++++++++++++++++++++++++++++++++++++++++
 	
    string line; 
	getline(infile, line);
	
	istringstream iss(line); // make fileline into stream
	// leggi dallo stream (solo 1a riga, ocio)
	iss >> number ;
		
	infile.close();
	
	if(number==0) cout << "OK" << endl;
	else cout << "number " << number << " !!! NOT OK!!! " << endl;
	
	return number;


}





