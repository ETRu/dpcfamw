#!/bin/bash

# Author: Elena Tea Russo @ Institute for Research and Technologies (Area Science Park) & SISSA. 
# Package: DPCfamW version 1.0
# Description: unittest of ./compute_dmat.sh
# USAGE: ./test_compute_dmat.sh  logfile outfile dataset
# where dataset can be either A B C or D 

# SPDX-License-Identifier: CC-BY-4.0

export LC_NUMERIC="en_US.UTF-8"

logfile=$1
outfile=$2
dataset=$3


date >> $logfile

Nblocks=`awk 'NR==2{print $1}' data/set${dataset}/configure `

# run command
cd ..
mkdir tests/distance_matrix
./compute_dmat.sh tests/data/set${dataset}/primary_clusters tests/distance_matrix $Nblocks >> tests/$logfile
cd -



EC=0
# check iterativeley "Blocks" outputs (i-j.bin)
for ((i=1;i<=Nblocks;++i)); do
for ((j=i;j<=Nblocks;++j)); do
 diff distance_matrix/${i}-${j}.bin data/set${dataset}/distance_matrix/${i}-${j}.bin &> tt
 check=`wc tt | awk '{print $3}'`
 if (( check == 0 )); then
	echo '(v) test: ' ${i}-${j}.bin 'ok' >> $logfile
	else
	echo '(X) test: ERROR on' ${i}-${j}.bin >> $logfile
	EC=$((EC + 1))
 fi
rm tt
rm distance_matrix/${i}-${j}.bin
done
done

echo "EC:" $EC >> $logfile
echo $EC > $outfile


rm -r distance_matrix

#if (($EC>0)); then
# exit $EC
#fi 
