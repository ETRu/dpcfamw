#!/bin/bash

# Author: Elena Tea Russo @ Institute for Research and Technologies (Area Science Park) & SISSA. 
# Package: DPCfamW version 1.0
# Description: unittest of ./prepare_dataset.sh
# USAGE: ./test_prepare_dataset.sh  logfile outfile dataset
# where dataset can be either A B C or D 

# SPDX-License-Identifier: CC-BY-4.0

export LC_NUMERIC="en_US.UTF-8"

logfile=$1
outfile=$2
dataset=$3

date >> $logfile

mkdir fastas

Nblocks=`awk 'NR==2{print $1}' data/set${dataset}/configure `

# run command

cd ..
./prepare_dataset.sh  tests/data/set${dataset}/proteins.fasta tests/dictionary.txt tests/proteins.fasta.numid tests/fastas $Nblocks >> tests/$logfile
cd -

# outputs list:

o_list="dictionary.txt proteins.fasta.numid.tab proteins.fasta.numid"

# check outputs in o_list
EC=0
for o in $o_list; do
 diff $o data/set${dataset}/$o &> tt
 check=`wc tt | awk '{print $3}'`
 if (( check == 0 )); then
	echo '(v) test: ' $o 'ok' >> $logfile
	else
	echo '(X) test: ERROR on' $o >> $logfile
	EC=$((EC + 1))
 fi
rm tt
rm $o
done

# check iterativeley "Blocks" outputs (fastas/i.fasta)
for ((i=1;i<=Nblocks;++i)); do
 diff fastas/${i}.fasta data/set${dataset}/fastas/${i}.fasta &> tt
 check=`wc tt | awk '{print $3}'`
 if (( check == 0 )); then
	echo '(v) test: ' fastas/${i}.fasta 'ok' >> $logfile
	else
	echo '(X) test: ERROR on' fastas/${i}.fasta >> $logfile
	EC=$((EC + 1))
 fi
rm tt
rm fastas/${i}.fasta
done


rm -r fastas

echo "EC:" $EC >> $logfile
echo $EC > $outfile




#if (($EC>0)); then
# exit $EC
#fi 