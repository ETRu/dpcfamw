#!/bin/bash

# Author: Elena Tea Russo @ Institute for Research and Technologies (Area Science Park) & SISSA. 
# Package: DPCfamW version 1.0
# Description: unittest of ./create_final_MCs.sh
# USAGE: ./test_create_final_MCs.sh  logfile outfile dataset
# where dataset can be either A B C or D 

# SPDX-License-Identifier: CC-BY-4.0

export LC_NUMERIC="en_US.UTF-8"

logfile=$1
outfile=$2
dataset=$3


date >> $logfile


Nblocks=`awk 'NR==2{print $1}' data/set${dataset}/configure `

# run command
cd ..
mkdir tests/metaclusters
./create_final_MCs.sh   tests/data/set${dataset}/metaclusters  tests/metaclusters tests/data/set${dataset}/proteins.fasta.numid.tab tests/data/set${dataset}/dictionary.txt  >> tests/$logfile -ltr
cd -




EC=0

o_list="filtered-labels_complete.txt MClist.txt MCstatistics.txt"

# check outputs in o_list
EC=0
for o in $o_list; do
 diff metaclusters/$o data/set${dataset}/metaclusters/$o &> tt
 check=`wc tt | awk '{print $3}'`
 if (( check == 0 )); then
	echo '(v) test: ' $o 'ok' >> $logfile
	else
	echo '(X) test: ERROR on' $o >> $logfile
	EC=$((EC + 1))
 fi
rm tt
rm metaclusters/$o
done



cd metaclusters/seeds
o_list=`ls`
cd -

# check outputs in o_list
EC=0
for o in $o_list; do
 diff metaclusters/seeds/$o data/set${dataset}/metaclusters/seeds/$o &> tt
 check=`wc tt | awk '{print $3}'`
 if (( check == 0 )); then
	echo '(v) test: ' $o 'ok' >> $logfile
	else
	echo '(X) test: ERROR on' $o >> $logfile
	EC=$((EC + 1))
 fi
rm tt
rm metaclusters/seeds/$o
done



echo "EC:" $EC >> $logfile
echo $EC > $outfile

rm -r metaclusters 

