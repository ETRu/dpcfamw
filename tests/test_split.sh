#!/bin/bash


# Author: Elena Tea Russo @ Institute for Research and Technologies (Area Science Park) & SISSA. 
# Package: DPCfamW version 1.0
# Description: unittest of split.x 
# USAGE: ./test_splitd.sh logfile outfile 

export LC_NUMERIC="en_US.UTF-8"

logfile=$1
outfile=$2
#no dataset. no third argument

indir=./data/test_split
mkdir data/test_tmp
outdir=./data/test_tmp


for N in 2 5 10; do 
echo "Splitting files in $N." >> $logfile

mkdir data/test_tmp/${N}
../bin/split.x ${indir}/proteins.fasta.numid.tab  ${outdir}/${N}/sproteins.fasta.numid.tab  $N >> $logfile
../bin/split.x ${indir}/C1.cl ${outdir}/${N}/sC1.cl  $N   >> $logfile
done

EC=0
for N in 2 5 10; do
for ((i=1;i<=N;++i)); do
inf=${outdir}/${N}/sproteins.fasta.numid.tab.${i}
reff=${indir}/${N}/sproteins.fasta.numid.tab.${i}
diff  $inf $reff  &> tt
check=`wc tt | awk '{print $3}'`
 if (( check == 0 )); then
	echo '(v) test: ' $inf 'ok' >> $logfile
	else
	echo '(X) test: ERROR on' $inf >> $logfile
	EC=$((EC + 1))
 fi
rm tt
done
done

for N in 2 5 10; do
for ((i=1;i<=N;++i)); do
inf=${outdir}/${N}/sC1.cl.${i}
reff=${indir}/${N}/sC1.cl.${i}
diff  $inf $reff  &> tt
check=`wc tt | awk '{print $3}'`
 if (( check == 0 )); then
	echo '(v) test: ' $inf 'ok' >> $logfile
	else
	echo '(X) test: ERROR on' $inf >> $logfile
	EC=$((EC + 1))
 fi
rm tt
done
done

echo "EC:" $EC >> $logfile
echo $EC > $outfile

rm -r ${outdir}






