#!/bin/bash

# Author: Elena Tea Russo @ Institute for Research and Technologies (Area Science Park) & SISSA. 
# Package: DPCfamW version 1.0
# Description: unittest of metaclustering with P53 datased
# USAGE: ./test_mc_withP53.sh  logfile outfile 

# SPDX-License-Identifier: CC-BY-4.0

export LC_NUMERIC="en_US.UTF-8"

logfile=$1
outfile=$2
#no dataset. no third argument

indir=./data/test_metacl_P53/data/P53dmat     
refdir=./data/test_metacl_P53/reference 
mkdir data/test_tmp
outdir=./data/test_tmp

# run metaclustering.x

../bin/metaclustering.x ${indir}/P53dmat_??.bin -o ${outdir}/res   

# remove unused files
rm peaksIdx.txt
rm distII.txt 
rm labels_before_merge.txt 

# split in 2 files with different columns for comparison:

awk 'NR<=3593{print $4}' ${outdir}/res  > ${outdir}/labels
awk 'NR<=3593{print $1, $2, $3}' ${outdir}/res  > ${outdir}/decisiongraph



# compare results to reference

EC=0

# compare labels
o=${outdir}/labels
ref=${refdir}/labels_aftermerge_ref.txt 
diff  $o $ref &> tt
check=`wc tt | awk '{print $3}'`
 if (( check == 0 )); then
	echo '(v) test: ' $o 'ok' >> $logfile
	else
	echo '(X) test: ERROR on' $o >> $logfile
	EC=$((EC + 1))
 fi
rm tt

echo "EC:" $EC >> $logfile
echo $EC > $outfile

# compare decision graph
o=${outdir}/decisiongraph
ref=${refdir}/DecisionGraph_ref.txt  
diff  $o $ref &> tt
check=`wc tt | awk '{print $3}'`
 if (( check == 0 )); then
	echo '(v) test: ' $o 'ok' >> $logfile
	else
	echo '(X) test: ERROR on' $o >> $logfile
	EC=$((EC + 1))
 fi
rm tt

echo "EC:" $EC >> $logfile
echo $EC > $outfile

rm -r ${outdir}






