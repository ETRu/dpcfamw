#!/bin/bash

# Author: Elena Tea Russo @ Institute for Research and Technologies (Area Science Park) & SISSA. 
# Package: DPCfamW version 1.0
# Description: unittest of distance matrix using P53 dataset 
# USAGE: ./test_fistIcl_withP53.sh logfile outfile

# SPDX-License-Identifier: CC-BY-4.0

export LC_NUMERIC="en_US.UTF-8"

logfile=$1
outfile=$2
#no dataset. no third argument

indir=./data/test_distIcl_P53/data_qsize/P53
refdir=./data/test_distIcl_P53/reference
mkdir data/test_tmp
outdir=./data/test_tmp

echo "Generating off diagonal elements.." >> $logfile
{
# off diagonal
start=0; end=2;
for i in $(seq ${start} ${end})
do
	for j in $(seq $((i+1)) ${end})
	do
   		../bin/dist_Icl_ND.x ${indir}/P53_primaryclusters_$i.bin ${indir}/P53_primaryclusters_$j.bin -o ${outdir}/dtmp_$i$j.bin 
   	done
done
}>> $logfile
echo "Done!" >> $logfile

echo "Generating diagonal elements.." >> $logfile
{
# diagonal
for i in $(seq ${start} ${end})
do
	../bin/dist_Icl_D.x ${indir}/P53_primaryclusters_$i.bin ${indir}/P53_primaryclusters_$i.bin -o ${outdir}/dtmp_$i$i.bin >> $logfile
done
}>> $logfile
echo "Done!" >> $logfile 

# COMPARE RESULTS FROM REFERENCE

find ${outdir} -name 'dtmp_??.bin' -exec bash -c '../bin/read_output.x $0 > ${0/.bin/.txt}'  {} \;

cat ${outdir}/dtmp_??.txt > ${outdir}/dtmp.txt 

sort -n -s -k2,2 ${outdir}/dtmp.txt | sort -n -s -k1,1 > ${outdir}/dtmp_sorted.txt

#cmp ${outdir}/dtmp_sorted.txt ${refdir}/wrongP53dmat_ref.txt >> ${outdir}/tt

EC=0

o=${outdir}/dtmp_sorted.txt
ref=${refdir}/P53dmat_ref.txt
diff  $o $ref &> tt
check=`wc tt | awk '{print $3}'`
 if (( check == 0 )); then
	echo '(v) test: ' $o 'ok' >> $logfile
	else
	echo '(X) test: ERROR on' $o >> $logfile
	EC=$((EC + 1))
 fi
rm tt

echo "EC:" $EC >> $logfile
echo $EC > $outfile

rm -r ${outdir}






