#!/bin/bash

# Author: Elena Tea Russo @ Institute for Research and Technologies (Area Science Park) & SISSA. 
# Package: DPCfamW version 1.0
# Description: unittest of ./blast-command.sh 
# USAGE: ./test_blast-command.sh  logfile outfile dataset
# where dataset can be either A B C or D 

# SPDX-License-Identifier: CC-BY-4.0

export LC_NUMERIC="en_US.UTF-8"

logfile=$1
outfile=$2
dataset=$3


date >> $logfile


Nblocks=`awk 'NR==2{print $1}' data/set${dataset}/configure `

# run command

cd ..
mkdir tests/alignments
./blast-command.sh  tests tests/data/set${dataset}/proteins.fasta.numid tests/data/set${dataset}/fastas tests/alignments $Nblocks >> tests/$logfile
cd -

# outputs list:

o_list="blastdb.phr  blastdb.pog  blastdb.psd  blastdb.psi  blastdb.psq"

# check outputs

EC=0
for o in $o_list; do
 diff $o data/set${dataset}/$o &> tt
 check=`wc tt | awk '{print $3}'`
 if [[ "$check" == 0 ]]; then
	echo '(v) test: ' $o 'ok' >> $logfile
	else
	echo '(X) test: ERROR on' $o >> $logfile
	EC=$((EC + 1))
fi
rm tt
rm $o
done

# check iterativeley "Blocks" outputs (i.blasted)
for ((i=1;i<=Nblocks;++i)); do
 diff alignments/${i}.blasted data/set${dataset}/alignments/${i}.blasted &> tt
 check=`wc tt | awk '{print $3}'`
 if (( check == 0 )); then
	echo '(v) test: ' ${i}.blasted 'ok' >> $logfile
	else
	echo '(X) test: ERROR on' ${i}.blasted >> $logfile
	EC=$((EC + 1))
 fi
rm tt
rm alignments/${i}.blasted
done


rm blastdb.pin
rm -r alignments

echo "EC:" $EC >> $logfile
echo $EC > $outfile 		