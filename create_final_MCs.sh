
#!/bin/bash

# Author: Elena Tea Russo @ Institute for Research and Technologies (Area Science Park) & SISSA. 
# Package: DPCfamW version 1.0
# Description: script to generate final MCs' SEEDS

# SPDX-License-Identifier: CC-BY-4.0

export LC_NUMERIC="en_US.UTF-8"

# folders ALWAYS without final /
MCinput_folder_wp=$1 # folder where  sequence-labels_*.bin is stored (metaclusters folder)
MCoutput_folder_wp=$2 # output folder (technically same as above but it can be different)
tabular_in_fasta_file_wp=$3 # original fasta transformed in numid.tab (generated with prepare_dataset)
dictionary_file_wp=$4 # dictionary file (generated with prepare_dataset)




# add sequences and protein names to the filtered file

 awk 'NF==2{seq[$1]=$2}NF>2{print $0, substr(seq[$1],$2,$3-$2+1)}' ${tabular_in_fasta_file_wp} ${MCinput_folder_wp}/filtered-labels_ALL.txt | awk 'length($5)<$3-$2+1 {print $1, $2, $3-1, $4, $5} length($5)==$3-$2+1 {print $0}' > ${MCoutput_folder_wp}/tt
 
 awk 'NF==1{name[NR]=$1}NF>1{print $1, name[$1], $2, $3, $4, $5}' ${dictionary_file_wp}  ${MCoutput_folder_wp}/tt  >  ${MCoutput_folder_wp}/filtered-labels_complete.txt 

 rm  ${MCoutput_folder_wp}/tt

# split in metaclusters seed fasta files
# make seed subfolder:

mkdir  ${MCoutput_folder_wp}/seeds 

# list of MCs:
awk '{print $5}'  ${MCoutput_folder_wp}/filtered-labels_complete.txt | awk '!seen[$1]++' | sort -g >  ${MCoutput_folder_wp}/MClist.txt                                
list=`cat ${MCoutput_folder_wp}/MClist.txt`

# create MCXXX.fasta seed files
for mc in $list; do awk -v mc=$mc '$5==mc{print ">" $2 "/" $3 "-" $4; print $6}'  ${MCoutput_folder_wp}/filtered-labels_complete.txt > ${MCoutput_folder_wp}/seeds/MC${mc}.fasta; done  

# compute relevant quantities, such as: size, average length, std-avlen
# initiate with  header to MCstatistics.txt
echo "#MC N_sequences average_length std_deviation_AL"  >  ${MCoutput_folder_wp}/MCstatistics.txt
# compute statistics
for mc in $list; do awk -v mc=$mc '$5==mc{size++; avlen+=$4-$3+1;  avlen2+=($4-$3+1)^2;}END{print mc, size, avlen/size, sqrt((avlen2/size) - (avlen/size)^2)}'  ${MCoutput_folder_wp}/filtered-labels_complete.txt; done >>  ${MCoutput_folder_wp}/MCstatistics.txt


 