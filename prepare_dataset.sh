#!/bin/bash

# Author: Elena Tea Russo @ Institute for Research and Technologies (Area Science Park) & SISSA. 
# Package: DPCfamW version 1.0
# Description: script to prepare input data for DPCfamW clustering

# SPDX-License-Identifier: CC-BY-4.0


export LC_NUMERIC="en_US.UTF-8"

input_fasta_file_wp=$1
dict_file_wp=$2
numeric_in_fasta_file_wp=$3
fastas_folder=$4
Nblocks=$5

tabfile_wp=${numeric_in_fasta_file_wp}".tab"

# generate tab with fasta without attributes in protein name
awk '$1~">"{printf "\n%s ",$1} $1!~">" {printf "%s",$1}'  ${input_fasta_file_wp} | awk 'NR>1{print}' > tmp

# separate protein names and shuffle to create dictionary
awk '{print substr($1,2)}' tmp | shuf --random-source=${input_fasta_file_wp} | awk '{print $1}' > ${dict_file_wp}

awk 'NF==1{numid[$1]=NR}NF==2{nameid=substr($1,2); print numid[nameid], $2;}'  ${dict_file_wp} tmp | sort -gk1 > ${tabfile_wp}

awk '{printf ">%s\n%s\n", $1,$2}' ${tabfile_wp}  >  ${numeric_in_fasta_file_wp}

# compute total number of sequences:
tot=`wc  ${tabfile_wp} | awk '{print $1}'`
htot=$(($tot/2)) 

# SPLIT IN BLOCKS: first check if the splitting is reasonable
if [ -z $Nblocks ]
then
 echo "\$Nblocks is empty! ABORTING. Check your configure file!"
 exit 1
fi

# check if it is an integer number
[ -n "$Nblocks" ] && [ "$Nblocks" -eq "$Nblocks" ] 2>/dev/null
if [ $? -ne 0 ]; then
   echo "$Nblocks is not an integer number! Check your configure file!"
   exit 1
fi

if [ 1 -gt $Nblocks ]; then
 echo "$Nblocks is not a valid number of blocks! Check your configure file!"
 exit 1
fi

if [ $Nblocks -gt $htot ]; then
 echo "$Nblocks is not a valid number of blocks (larger than $tot / 2)! Check your configure file!"
 exit 1
fi


# SPLIT IN BLOCKS: split

if [ $Nblocks -gt 1 ]; then
echo "Running splitting program"
# run split program
bin/split.x  ${tabfile_wp} ${fastas_folder}/tmp $Nblocks
# convert files into fastas:
cd ${fastas_folder}
for ((i=1;i<=Nblocks;++i)); do
awk '{printf ">%s\n%s\n", $1,$2}' tmp.$i > $i.fasta
rm tmp.$i
done
cd -
else
echo "Only 1 block, no need to run the splitting program: just copying"
cp ${numeric_in_fasta_file_wp} ${fastas_folder}/1.fasta
fi

rm tmp
