
#!/bin/bash

# Author: Elena Tea Russo @ Institute for Research and Technologies (Area Science Park) & SISSA. 
# Package: DPCfamW version 1.0
# Description: wrapper for filtering.x

# SPDX-License-Identifier: CC-BY-4.0

export LC_NUMERIC="en_US.UTF-8"

# folders ALWAYS without final /
MCinput_folder_wp=$1 # folder where  sequence-labels_*.bin is stored (metaclusters folder)
MCoutput_folder_wp=$2 # output folder (technically same as above but it can be different)
Nblocks=$3    #not really needed in this case, we use it to decide how many output to print, but it has not to be consistent actually. Still, we do this.

# check if Nblocks variable is reasonable
if [ -z $Nblocks ]
then
 echo "\$Nblocks is empty! ABORTING. Check your configure file!"
 exit 1
fi

[ -n "$Nblocks" ] && [ "$Nblocks" -eq "$Nblocks" ] 2>/dev/null
if [ $? -ne 0 ]; then
   echo "$Nblocks is not an integer number! Check your configure file!"
   exit 1
fi

if [ 1 -gt $Nblocks ]; then
 echo "$Nblocks is not a valid number of blocks! Check your configure file!"
 exit 1
fi

# actually at this point Nblocks is not a useful number anymore. We first need to know how many files sequence-labels_*.bin there are in the mteaclusters folder

Nfiles=`ls ${MCinput_folder_wp}/sequence-labels_*.bin | wc | awk '{print $1}' `

echo "I found " $Nfiles "files to process"	

# run command over all files in Nfiles

for ((i=1;i<=Nfiles;++i)); do
time bin/filtering.x ${MCinput_folder_wp}/sequence-labels_${i}.bin -o ${MCoutput_folder_wp}/filtered-labels_${i}.txt
done

for ((i=1;i<=Nfiles;++i)); do
cat ${MCoutput_folder_wp}/filtered-labels_${i}.txt
done >  ${MCoutput_folder_wp}/filtered-labels_ALL.txt
 

for ((i=1;i<=Nfiles;++i)); do
rm ${MCoutput_folder_wp}/filtered-labels_${i}.txt
done