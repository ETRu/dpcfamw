#!/bin/bash

# Author: Elena Tea Russo @ Institute for Research and Technologies (Area Science Park) & SISSA. 
# Package: DPCfamW version 1.0
# Description: wrapper for primary clustering code

# SPDX-License-Identifier: CC-BY-4.0

export LC_NUMERIC="en_US.UTF-8"

blasted_infolder_wp=$1
output_folder_wp=$2
Nblocks=$3

# check if Nblocks variable is reasonable
if [ -z $Nblocks ]
then
 echo "\$Nblocks is empty! ABORTING. Check your configure file!"
 exit 1
fi

[ -n "$Nblocks" ] && [ "$Nblocks" -eq "$Nblocks" ] 2>/dev/null
if [ $? -ne 0 ]; then
   echo "$Nblocks is not an integer number! Check your configure file!"
   exit 1
fi

if [ 1 -gt $Nblocks ]; then
 echo "$Nblocks is not a valid number of blocks! Check your configure file!"
 exit 1
fi


# check is there is alredy an log ouput file, if yes delete since we will append
if [ -f "${folder}/Icl.log" ] 
then 
	rm ${folder}/Icl.log
	echo "WARNING: old log file found. OLD LOG FILE REMOVED"
fi





# cycle over Nblocks 

for ((i=1;i<=Nblocks;++i)); do

# check is there is alredy an ouput file, if yes delete it since c++ program will append
if [ -f "${output_folder_wp}/${i}.cl" ] 
then 
	rm ${output_folder_wp}/${i}.cl
	echo "WARNING: old file ${output_folder_wp}/${i}.cl found. OLD FILE REMOVED"
fi

time bin/primaryclustering.x ${blasted_infolder_wp}/${i}.blasted ${output_folder_wp}/${i}.cl 0.2 0.5  >> ${output_folder_wp}/Icl.log

done


 