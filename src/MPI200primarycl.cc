
// Author: Elena Tea Russo @ Institute for Research and Technologies (Area Science Park) & SISSA.
// Package: DPCfamW version 1.0
// Description: MPI VERSION OF C++ program to perform primary clustering of local pairwise alignments on a single query.
// USAGE:  ./a.out  input_FOLDER  output_FOLDER dpar gpar START_fileID
// input is a BLAST output obtained from:
//	blastp  -query $myquery -db $mydb -evalue 0.1 -max_target_seqs 200000 -out $myout -outfmt "6 qseqid sseqid qstart qend sstart send qlen slen length pident evalue score"

// SPDX-License-Identifier: CC-BY-4.0


#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <vector>
#include <iostream>
#include <fstream>
#include <sstream>
# include <cstdlib>
# include <ctime>
# include <iomanip>
# include <mpi.h>


using namespace std;





//---------------------------------------------------------------------------------------------------------
// DEFINITION OF AN ALIGNED SEGMENT 
//---------------------------------------------------------------------------------------------------------
struct segment {	
  // SEARCH and QUERY ids (in numbers, no txts!)
  int sid;
  int qid;	
  //  alignment START and END on the QUERY
  int qstart;
  int qend;
  //  alignment START and END on the SEARCH
  int sstart;
  int send;
  //total alignment length
  int len;
  //roughscore
  int score;
  // rho and delta for DPC  
  double rho, delta;
  // evalue 
  double evalue;
  // is this alignment a cluster center?
  int isc;
} ;




//---------------------------------------------------------------------------------------------------------
// SEGMENTS DISTANCE on the QUERY
//--------------------------------------------------------------------------------------------------------- 
double dist(segment i, segment j){
	int hi, lo;
	double inte, uni;
	int istart, iend, jstart, jend;
    istart= i.qstart; iend= i.qend; jstart=j.qstart; jend=j.qend;
    //calculate intersection
    inte=0;
	hi=iend;
    if(jend<hi) hi=jend;
    lo=istart;
    if(jstart>lo) lo=jstart;
    if(hi>lo) inte=hi-lo;
    //calculate union
    hi=iend;
    if(jend>hi) hi=jend;
    lo=istart;
    if(jstart<lo) lo=jstart;
    uni=hi-lo;
return (uni-inte)/uni;
}

//---------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------
/////////////////////////////////   M   A   I   N    //////////////////////////////////////////////////////
//---------------------------------------------------------------------------------------------------------
//---------------------------------------------------------------------------------------------------------





int main(int argc, char** argv) {

	// MPI INIZIALIZATION 
	int rank;
	MPI_Init(&argc,&argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);

	ifstream infile; // input 
	ofstream outfile; // output

	double dpar; // parameter for the density estimation kernerl
	double gpar; // parameter for gap search
	dpar=atof(argv[3]);
	gpar=atof(argv[4]);
	
	int nalldata;
	vector<segment> allsegments;


	// SET INPUT AND OUTPUT FILENAMES (FOR PARALLEL MPI EXECUTION)
	int START_fileID;
	int MY_fileID;	
	string infile_path, outfile_path;
	stringstream sstm;

	// define current id (passi di 40000) XXX
	START_fileID=atoi(argv[5]);
	MY_fileID=START_fileID+rank*40000;
	// input file
	sstm << argv[1] << MY_fileID <<  ".blasted";
	infile_path = sstm.str();
	// outputf ile
	sstm.str("");
	sstm << argv[2] << MY_fileID <<  ".cl";
	outfile_path = sstm.str();


	cout << "\n \n    Input File:        " << infile_path << endl;
	cout << "\n \n    Output File:        " << outfile_path << endl;






	infile.open (infile_path.c_str());
	if (!infile.is_open()) { cout << "INFILE" << infile_path << " NOT GOOD" << endl; return 1; }
	//cout << "\n \n    Input File:        " << infile_path  << endl;
 
 	
 
    // ++++++++++++++++++++++++++++++++++ READ FROM FILE +++++++++++++++++++++++++++++++++++++++++++++++++++
 	
    string line; 
	while( getline(infile, line)) {
		int qseqid, sseqid;  // id numerico
		int qstart, qend, sstart, send, length;
		double evalue;
		int score;
		// data that I am not currently using.
    	int  qlen, slen;
		double pident;
    	
		istringstream iss(line); // make fileline into stream
		// leggi dallo stream (controllando IDTYPE)
		iss >> qseqid >> sseqid >> qstart >> qend >> sstart >> send >> qlen >> slen >> length >> pident >> evalue >> score;
		//generate a temporary segment that i will then append to the segments vector	
		segment temp;
 		temp.qid=qseqid;
		temp.sid=sseqid;
		temp.qstart=qstart;
		temp.qend=qend+1;
		temp.sstart=sstart;
		temp.send=send+1;
		temp.len=length;
		temp.score=score;
		temp.evalue=evalue;
		temp.rho=0;
		temp.delta=0;
		temp.isc=0;
		
		allsegments.push_back(temp);
		
	}
	
	infile.close();
    
	nalldata=allsegments.size();
    
 	// ++++++++++++++++++++++++++++++++++ END READ FROM FILE +++++++++++++++++++++++++++++++++++++++++++++++++++
 	    
	//cout << "*** Segmenti TOTALI letti : " << allsegments.size() << endl;

	//cout << "*** Prima QID:" << allsegments[0].qid << endl;
	//cout << "*** Ultima QID:" << allsegments[allsegments.size()-1].qid << endl;


    // ---  EXTRACT QUERY-SPECIFIC ALIGNMENTS (NOTE THAT DATA ARE ALREASY SORTED BY QUERY, THUS IT IS EASY)

	int thisquery, begin, end;
	begin=0; end=-1;
	
	//cout<< " [[[ Iniziamo: " << end << " <? " << nalldata << " ]]] " << endl;

	while ( end < nalldata-1 ) {

	//select the alignments 
	begin=end+1;
        thisquery=allsegments[begin].qid;
	//cout << "This query is query " << thisquery << endl;
	//cout << "Begin is " << begin << " and End is (was) " << end << endl;
	for(int i=begin; i<nalldata; ++i){
		//cout<< i << " " << allsegments[i].qid << endl;
		if(allsegments[i].qid != thisquery) {end=i-1; break;}
		if(i==nalldata-1){end=nalldata-1;}
	}


	// copy the selected package of alignments with the same query in the new vector
	vector<segment> mysegments;
	copy(allsegments.begin()+begin, allsegments.begin()+end+1, back_inserter(mysegments)); 

	//cout << "----- BEGIN " << begin << " END " << end << endl;
	cout << "Segmenti locali letti : " << mysegments.size() << endl;
	//cout << "QID:" << thisquery << endl;
	//cout << "Prima QID:" << mysegments[0].qid << endl;
	//cout << "Ultima QID:" << mysegments[mysegments.size()-1].qid << endl;


	// DO NOT ANALYISE QUERIES WITH LESS THAN 20 ALIGNMENTS
	if(mysegments.size()>20){



//////////////////////////////////////////////////////// FROM HERE
	
	cout << " ...     ANALIZZO  ......................................." << endl;



    vector<int> centres;
    int ndata;
    int n_putative_centres;


    // CLEANING SUPERPOSITIONS ON THE SAME ALIGNMENTS
   
   	vector<int> todel;    
	for(int i=0; i<mysegments.size();++i) {
	    for(int j=i+1; j<mysegments.size();++j) {
	    	if(mysegments[i].sid == mysegments[j].sid){
	    			if( dist( mysegments[i], mysegments[j]) < dpar ) {
	    			  //check if it is alterdy there
	    			  int repetita=0;
	    			  int indextodel;
	    			  if(mysegments[i].score > mysegments[j].score) { indextodel=j; }
	    			  else { indextodel=i; }
	    			  for(int k=0; k<todel.size(); ++k) {if(todel[k] == indextodel) repetita=1;}
	    			  if(repetita==0){
	    					todel.push_back(indextodel);
	    			  }
	    			}
	    	}
	    }
    	}

     
    //cout << "   TOO MUCH SELF-OVERLAP ON :" << todel.size() << " ELEMENTS\n   REMOVING: ";
    //for(int td=0; td<todel.size(); ++td) { cout << todel[td] << " "; }
    //cout << endl;
	for(int td=todel.size()-1; td>=0; --td ){  mysegments.erase (mysegments.begin()+td);  }
	cout << "- segmenti effettivi : " << mysegments.size() << endl;   
    //if (mysegments.size()<20) return 1;


    ndata =  mysegments.size();

    // DEFINE NUMBER OF PUTATIVE CENTRES IN FUNCTION OF THE NUMBER OF DATA, IF MORE THAN 200 DATA NPC IS 20, OTHERWISE IT IS NDATA/200
    if (ndata>200) n_putative_centres=20;
    else n_putative_centres=ndata/10;
    if(n_putative_centres<2) n_putative_centres=2;
   
    // -----------------------------------------------------------------------------------------------------------------------    
    // COMPUTE RHO
    
    //density inizialization
    for(int i=0;i<mysegments.size();++i) { 
        int seed;
        seed=mysegments[i].sid+mysegments[i].qstart+mysegments[i].qend-mysegments[i].sstart*mysegments[i].send;
        srand(seed); 
        mysegments[i].rho = 1 + 0.1*((double) rand() / (RAND_MAX));
        //cout << i << " " << seed << " " << mysegments[i].rho<<endl;
    }   

    for(int i=0; i<mysegments.size();++i) { 
     	for(int j=i+1; j<mysegments.size();++j){
    		double d;
   			d = dist( mysegments[i], mysegments[j]);// + 0.00001*((double) rand() / (RAND_MAX));
   			// stepwise kernel (large as dpar)
   			if(d<dpar) { mysegments[i].rho += 1; mysegments[j].rho += 1;}
    	}
    }

   

    // -----------------------------------------------------------------------------------------------------------------------    
    // COMPUTE DELTA
        
    for(int i=0; i<mysegments.size();++i) {
        //set the highest value of delta, this will be the value of the higher density peak
    	mysegments[i].delta=1000;
    	for(int j=0; j<mysegments.size();++j){
    		if(i==j) continue;
    		if(mysegments[j].rho <= mysegments[i].rho) continue;
    		double d;
    		d = dist( mysegments[i], mysegments[j]) + 0.00001*((double) rand() / (RAND_MAX));
    		if( d < mysegments[i].delta ) mysegments[i].delta=d; 
    		
    	}
    	
    }
    
   
   
   
   
    // -----------------------------------------------------------------------------------------------------------------------    
    //  SEARCH FOR THE FIRST n_putative_centres "PUTATITE CENTRES", i.e. those with higher gamma=delta*rho
   for(int npc=0; npc<n_putative_centres; ++npc){
	
		double maxgamma=0;
		int maxi=-1;
		for(int i=0; i<mysegments.size();++i) {
		 double gamma; 
		 gamma = mysegments[i].rho * mysegments[i].delta;
		 if( (gamma > maxgamma) && mysegments[i].isc !=1 ) {maxgamma = gamma; maxi=i;}
		}
		mysegments[maxi].isc=1;
		//put the index of the center in a specific vector
		centres.push_back(maxi);
		
    }

    // -----------------------------------------------------------------------------------------------------------------------    
    //  FIND "TRUE" CENTER USING THE GAP CRITERION, i.e. serch for the last "gap" between points (wrt gamma) such that -log(gamma2/gamma1) > gpar 
    
    int lastgap=0;
	for(int c=0; c<centres.size()-1; ++c){
			double gamma1 = mysegments[ centres[c] ].rho * mysegments[ centres[c] ].delta;
			double gamma2 = mysegments[ centres[c+1] ].rho * mysegments[ centres[c+1] ].delta;
			if( -log(gamma2/gamma1) > gpar ) lastgap=c;
			//cout << c << " " << centres[c] << " " << lastgap << " gamma1 " << gamma1 << " gamma2 " << gamma2 << " LOGgamma1 " << log(gamma1) << " LOGgamma2 " << log(gamma2) << "gamma1 " << gamma1 << " log(gamma2/gamma1) " << log(gamma2/gamma1) << endl;
	
	}    
	 
	
	// delete all centres after the gap
    for(int c=lastgap+1; c<centres.size(); ++c){
  	  //cout<< "cleaning " << centres[c] << endl; 
	  mysegments[centres[c]].isc=0;
    }
    centres.erase(centres.begin()+lastgap+1, centres.begin()+centres.size());
  	
  	
	//cout << " lastgap " << lastgap << endl;
	//for(int c=0; c<centres.size(); ++c) cout << centres[c] << " " ;
  	
  	
   
   

    // -----------------------------------------------------------------------------------------------------------------------    
    //  CLUSTERING
    
    outfile.open (outfile_path.c_str(),ios::app); // OPENING FINAL FILE
	if (!outfile.is_open()) { cout << "OUTFILE " << outfile_path << " NOT GOOD" << endl; return 1; }
    
    int incl=0;
    for(int i=0; i<mysegments.size(); ++i){
    	double mind=1000001;
    	int cl=-1;
    	//search for the closest center to the segment
        if(mysegments[i].isc==1) {
            for(int c=0; c<centres.size(); ++c){
        		if(centres[c]==i) cl=c;
    	      }
        }
        else{
            for(int c=0; c<centres.size(); ++c){
        		double d = dist( mysegments[i], mysegments[centres[c]]) ;//+ 0.00001*((double) rand() / (RAND_MAX));
    	    	if(d < mind) { mind=d; cl=c; }
    	      }
        }    
        //assagint to it (if it exist)
    	if(cl>-1){
    	double d = dist( mysegments[i] , mysegments[ centres[cl] ]) ;
    	//print output; d<dpar set ok=1, otherwise ok=0;
    	  int cent;
    	  if( d >= dpar ){cl=-1, cent=-1;} else { cent=centres[cl] ; ++incl;}
    	  outfile << 
	  thisquery << " " <<
    	  i << " " <<  
    	  cent << " " << 
    	  cl << " "  << 
    	  d << " " << 
    	  mysegments[i].sid << " " << 
    	  mysegments[i].sstart << " " << 
    	  mysegments[i].send << " " <<
    	  mysegments[i].qstart << " " << 
    	  mysegments[i].qend << " " << 
    	  mysegments[i].score<< " " <<
          mysegments[i].evalue << " " <<
          mysegments[i].isc << " " <<
          mysegments[i].rho << " " <<
          mysegments[i].delta << " " <<
    	  endl;
    	   
    	 
    	 
    	}
    	else {
    	cout << " \n\nERROR CUSTERIZING" << endl;
    	return 2;    	
    	}
    
    }
   
   outfile.close();

   //cout << "\n\n INCL=" << incl << " NDATA=" << ndata << " diff:" << ndata-incl << " ratio:" << double(incl)/ndata << endl; 

   //cout << endl;   

//////////////////////////////////////////////////////// TO HERE

	}

	} // END WHILE

   // CLOSE MPI
   MPI_Finalize();


   return 0;


}





