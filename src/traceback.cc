
// Author: Federico Barone @ Institute for Research and Technologies (Area Science Park) & SISSA.
// Package: DPCfamW version 1.0
// Description: c++ program to filter relevant sequences of metaclusters after traceback

// SPDX-License-Identifier: CC-BY-4.0


/*******************************************************************************
* Traceback the MC labels from the primary clusters to its correspondent protein domain
* 
* Input format [binary]: queryID+center, searchID, searchStart, searchEnd.
* MC labels format [txt]: MC_labels
* Output format [binary]: sequenceLabel struct - searchID, search start, search end, MC label.;
* 
******************************************************************************/


#include <algorithm>
#include <cstring> // memcpy
#include <fstream>
#include <iostream>
#include <iterator>
#include <vector>
#include <list>
#include <random>
#include <functional>
#include <math.h>    
#include <unistd.h> // getopt
 
// #include "datatypes.h"


#define MAX_qID 4000
// #define MAX_qID 2353198020


//-------------------------------------------------------------------------
// Data types
//-------------------------------------------------------------------------


struct SequenceLabel
{
    uint32_t sID;
    uint16_t sstart;
    uint16_t send;
    int label;
    
    SequenceLabel(uint32_t s, uint16_t ss, uint16_t se, int l): sID(s), sstart(ss), send(se), label(l) {}
    SequenceLabel(): sID(0), sstart(0), send (0), label(-9) {}

};

// needed for std::upper_bound in compute_cluster_size
struct compare_label
{
    bool operator() (const SequenceLabel & left, const SequenceLabel & right)
    {
        return left.label < right.label;
    }
    bool operator() (const SequenceLabel & left, int right)
    {
        return left.label < right;
    }
    bool operator() (int left, const SequenceLabel & right)
    {   
        return left < right.label;
    }
};


// print a ClusteredAlignment TO STDOUT
inline void printSL( const SequenceLabel & sl) 
{
    std::cout <<  sl.sID << " " << sl.sstart << " " << sl.send <<  " " << sl.label << std::endl;   
}



// Small Clustered Alignment structure (only essential data...)
struct SmallCA
{
    uint32_t qID; // qID*100 + center
    uint32_t qSize;
    uint32_t sID;
    uint16_t sstart;
    uint16_t send;
    
    SmallCA(uint32_t q, uint32_t qs, uint32_t s, uint16_t ss, uint16_t se): qID(q), qSize(qs), sID(s), sstart(ss), send(se){}
    SmallCA(): qID(0), qSize(0),sID(0), sstart(0), send (0) {}

};


// needed for std::upper_bound in compute_cluster_size
struct compare_qID
{
    bool operator() (const SmallCA & left, const SmallCA & right)
    {
        return left.qID < right.qID;
    }
    bool operator() (const SmallCA & left, uint32_t right)
    {
        return left.qID < right;
    }
    bool operator() (uint32_t left, const SmallCA & right)
    {   
        return left < right.qID;
    }
};



struct compare_sID // used by auxiliary kmerge_binary
{
    bool operator() (const SmallCA & left, const SmallCA & right)
    {
        return left.sID < right.sID;
    }
    bool operator() (const SmallCA & left, uint32_t right)
    {
        return left.sID < right;
    }
    bool operator() (uint32_t left, const SmallCA & right)
    {   
        return left < right.sID;
    }
};

// print a ClusteredAlignment TO STDOUT
inline void printSCA( const SmallCA & clusterAlign) 
{
    std::cout << clusterAlign.qID << " " << clusterAlign.qSize << " " << clusterAlign.sID << " " << clusterAlign.sstart << " " << clusterAlign.send << std::endl;   
}


//-------------------------------------------------------------------------
// Functions
//-------------------------------------------------------------------------


void radix_sort(unsigned char * pData, uint64_t count, uint32_t record_size, 
                            uint32_t key_size, uint32_t key_offset)
{
    typedef uint8_t k_t [key_size];
    typedef uint8_t record_t [record_size];

    // index matrix
    uint64_t mIndex[key_size][256] = {0};            
    unsigned char * pTemp = new unsigned char [count*sizeof(record_t)];
    unsigned char * pDst, * pSrc;
    uint64_t i,j,m,n;
    k_t u;
    record_t v;

    // generate histograms
    for(i = 0; i < count; i++)
    {         
        std::memcpy(&u, (pData + record_size*i + key_offset), sizeof(u));
        
        for(j = 0; j < key_size; j++)
        {
            mIndex[j][(size_t)(u[j])]++;
            // mIndex[j][(size_t)(u & 0xff)]++;
            // u >>= 8;
        }       
    }
    // convert to indices 
    for(j = 0; j < key_size; j++)
    {
        n = 0;
        for(i = 0; i < 256; i++)
        {
            m = mIndex[j][i];
            mIndex[j][i] = n;
            n += m;
        }       
    }

    // radix sort
    pDst = pTemp;                       
    pSrc = pData;
    for(j = 0; j < key_size; j++)
    {
        for(i = 0; i < count; i++)
        {
            std::memcpy(&u, (pSrc + record_size*i + key_offset), sizeof(u));
            std::memcpy(&v, (pSrc + record_size*i), sizeof(v));
            m = (size_t)(u[j]);
            // m = (size_t)(u >> (j<<3)) & 0xff;
            std::memcpy(pDst + record_size*mIndex[j][m]++, &v, sizeof(v));
        }
        std::swap(pSrc, pDst);
        
    }
    delete[] pTemp;
    return;
}



/*******************************************************************************
* Loads binary file into std::vector<T>
*
* @param filename full path to file
* @param vector std::vector to store the content of the file
******************************************************************************/
template<typename T>
int load_labels(std::string filename, std::vector<T> & vector)
{
	int value;
    std::ifstream inFile (filename);
    
    vector.reserve(MAX_qID);


	// test file open   
	if (inFile) 
	{        
	    // read the elements in the file into a vector  
	    while ( inFile >> value ) 
	    {
	        vector.emplace_back(value);
	    }
	}
    else 
    {
        throw std::system_error(errno, std::system_category(), "failed to open "+ filename);
    }

   
    inFile.close();

    return 0;
}


std::vector<SequenceLabel> match_search_label(SmallCA * clusterAlign,  std::vector<int> &label, const uint64_t length)
{
    // Note that as implemented, all values have a label even if they are not present in the dmatII
    auto end = clusterAlign + length;
    auto low = clusterAlign;

    std::vector<SequenceLabel> searchLabel;
    searchLabel.reserve(length);

    while (low != end)
    {
        uint32_t val = low->qID;

        // find last occurrence
        auto high = std::upper_bound(low, end, val, compare_qID());

        // compute the difference
        auto count = high - low;

        // add cluster size to the list
        for (auto p = low; p < high; ++p)
        {
            // we use val-1 instead of val=qID because the label array is shifted by one (qID 0 is not possible)
            if (label[val-1]<0){continue;} // skip non assign values
            searchLabel.emplace_back(SequenceLabel(p->sID, p->sstart, p->send, label[val-1])    ); //  
        }

        // move to next element in vector (not immediate next)
        low = low + count;
    }

    // std::cout << "low-clusterAlign: " << low-clusterAlign << '\n';
    // std::cout << "searchLabel.size(): " << searchLabel.size() << '\n';

    return searchLabel;
}


int main(int argc, char *argv[])
{

	// std::string labelFilename = "/home/fbarone/win_doc/dpcfam/dpcfamw/globaltest/data/metaclusters/MClabels.txt";
    // std::string labelFilename = "/home/fbarone/win_doc/dpcfam/dpcfamw/tests/data/setA/MClabels.txt";


	SmallCA * bufferCA;

    // label vector: cluster label for each node
    std::vector<int> label;
    std::vector<SequenceLabel> searchLabel;



    //-------------------------------------------------------------------------
    // Argument parser

    int opt;
    std::string outputPath{"./"}; 
    std::string labelFilename{""}; 
    std::vector<std::string> filenames;
    int num_output_files = 0;

    while ((opt = getopt(argc, argv, "l:n:ho:")) != -1) 
    {
        switch (opt) 
        {
        case 'n':
            num_output_files = std::atoi(optarg);
            break;
        case 'l':
            labelFilename = optarg;
            break;
        case 'o':
            outputPath = optarg;
            // TODO: check for missing /
            break;
        case 'h':
            // go to default

        default: /* '?' */
            std::cerr << "Usage: \n";
            std::cerr << "\t " << argv[0] << " INPUT1 INPUT2 ... -l MC_LABELS [-o OUTPUT_PATH] [-n NUM_OUTPUT] \n\n";
            std::cerr << "\t INPUT              input filenames \n\n";
            std::cerr << "\t -l MC_LABELS       file containing a MC label for each primary cluster. \n\n";
            std::cerr << "\t -o OUTPUT_PATH     output path. \n\n";
            std::cerr << "\t -n NUM_OUTPUT      **if possible** output will be divided into NUM_OUTPUT files. \n\n";

            std::cerr << "Description:\n\t" << argv[0] << " assigns an MC label to each domain inside a primary cluster. \n\n";
            std::cerr << "\t It takes as input the content of all primary clusters (same input as dist_Icl) plus a file \n containing the labels for each primary cluster.\n";
            std::cerr << "\t It outputs all domains with its corresponding MC label. \n";
            std::cerr << "\t NOTE: the program will try to divide the output into NUM_OUTPUT files conditioned by not spliting \n an MC label in different files.\n\n";
            std::cerr << "\t Input format [binary]: queryID+center, searchID, searchStart, searchEnd.\n";
            std::cerr << "\t MC labels format [txt]: MC_labels.\n";
            std::cerr << "\t Output format [binary]: sequenceLabel struct - searchID, search start, search end, MC label. \n\n";
            exit(EXIT_FAILURE);
        }
    }


    // input checks
    if (labelFilename == "")
    {
        std::cerr << "Error: missing MC labels file (-l MC_LABELS)" << "\n";
        exit(EXIT_FAILURE);
    }

    if (optind >= argc) {
        std::cerr << "Error: missing input file." << "\n";    
        exit(EXIT_FAILURE);
        
    }

    // TODO: check outpath exists


    if (num_output_files > 100)
    {
        std::cerr << "Error: maximum number of output files is 100." << "\n";    
        exit(EXIT_FAILURE); 
    }


    while(optind < argc)
    {
        filenames.push_back(argv[optind]);
        std::cerr << "Input " << filenames.size() << " = " << argv[optind] << "\n";
        ++optind;
    }

    // if num_output_files not defined, default to filenames.size()
    if (num_output_files == 0)
    {
        num_output_files = filenames.size();
    }

    std::cerr << "Total input files: " << filenames.size() << "\n";    
    
    //-------------------------------------------------------------------------


    std::vector<SmallCA*> indexes; 
    std::vector<std::ifstream> files; 
    std::vector<uint64_t> fileLines; 
    uint64_t bytes{0};
    uint64_t lines{0};
    uint64_t totalLines{0};
    
    // read all files sizes to allocate total memory
    for (const auto & filename: filenames)
    {
        std::ifstream file (filename, std::ifstream::binary);
        if(file.is_open())
        {
            file.seekg (0, file.end);
            bytes = file.tellg();
            lines = bytes/sizeof(SmallCA);
            // std::cout << lines << std::endl;
            fileLines.push_back(lines);
            totalLines += lines;
            file.seekg (0, file.beg);
            files.push_back(std::move(file));
        }
        else{
            std::cerr << "Error: file " << filename << " missing or corrupted.\n";
            exit(EXIT_FAILURE);
        }
    }
    
    // allocate total buffer
    bufferCA = new SmallCA [totalLines];
    // populate buffer with each file
    auto fileBuffer = bufferCA;
    for (uint64_t i = 0; i < files.size(); ++i)
    {
        // std::cout << "indexes: " << indexes.size() << std::endl;
        files[i].read ((char*) fileBuffer, sizeof(SmallCA)*fileLines[i]);
        if (!files[i])
        {
            std::cout << "Error reading file " << filenames[i] << ": only " << files[i].gcount() << " could be read.";
        }
        files[i].close();
        fileBuffer += fileLines[i];
    }

    // load_labels
    load_labels(labelFilename, label);
    // std::cout << "label.size(): " << label.size() << '\n';
    // for (int i = 0; i < 10; ++i)
    // {
    //	std::cout << label[i] << std::endl;
    // }



    // sort wrt qID+center
    if (    !std::is_sorted( bufferCA, bufferCA+totalLines, compare_qID() )    )
    {   
        radix_sort((unsigned char*) bufferCA, totalLines, 16, 4, 0);
    }

    std::cout << "Check if file is sorted wrt qID: " << std::is_sorted(bufferCA, bufferCA+totalLines, compare_qID()) << std::endl;

    // match_search_label
    searchLabel = match_search_label(bufferCA, label, totalLines);
    
    delete [] bufferCA;

    // sort wrt to sID
    radix_sort((unsigned char*) searchLabel.data(), searchLabel.size(), 12, 4, 0);
    
    // sort wrt label
    // TODO: solve future source of bug!!:
    // I am abusing the use of radix_sort since it doesn't handle negative values!
    // the only negative label is -9 which translates to: 4294967287.
    radix_sort((unsigned char*) searchLabel.data(), searchLabel.size(), 12, 4, 8);

    // Sequence label sorted wrt label
    std::cout << "Check if file is sorted wrt label: " << std::is_sorted(searchLabel.begin(), searchLabel.end(), compare_label()) << std::endl;


    // split output into different files ordered by sID and metaclusters
    size_t avgLines = size_t(searchLabel.size() / num_output_files);
    // std::cout << "num_output_files: " << num_output_files << '\n';
    // std::cout << "avgLines: " << avgLines << '\n';
    // std::cout << "searchLabel.size(): " << searchLabel.size() << '\n';
    auto localStartIt = searchLabel.begin();
    auto localEndIt = localStartIt;


    for (int i = 1; i <= num_output_files; ++i)
    {
        if (    localEndIt == searchLabel.end()     )
        {
            num_output_files = i-1;
            break;
        }
    	std::string outFilename = outputPath + "sequence-labels_" + std::to_string(i) + ".bin";
    	// std::cout << "outFilename: " << outFilename << '\n';
    	std::ofstream outFile(outFilename, std::ofstream::binary);
		

        if ( (searchLabel.end() - (localStartIt + avgLines)) <= 0   )
        {
            localEndIt = searchLabel.end();
        }
        else
        {
            localEndIt = std::upper_bound(localStartIt, searchLabel.end(), (localStartIt + avgLines)->label, compare_label());
            if ( searchLabel.end() - localEndIt <= avgLines*.25)
            {
                localEndIt = searchLabel.end();
            }
        }

    	// auto diff = localEndIt - localStartIt;
    	// std::cout << "diff: " << diff << '\n';
    	// std::cout << "localStartIt->label: " << localStartIt->label << '\n';
        // std::cout << "localEndIt->label: " << (localEndIt-1)->label<< '\n';

		outFile.write((char*)&(*localStartIt), (localEndIt - localStartIt) * sizeof(SequenceLabel));
		localStartIt = localEndIt;
    	
    	outFile.close();
    }


    std::cerr << "Total output files: " << num_output_files << "\n";    
    std::cerr << "Done! \n";    

    return 0;

}
