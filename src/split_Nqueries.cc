// Author: Elena Tea Russo @ Institute for Research and Technologies (Area Science Park) & SISSA.
// Package: DPCfamW version 1.0
// Description: c++ program to filter relevant sequences of metaclusters after traceback
// USAGE:  ./a.out  infile    output_filename_root    Nfiles

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <vector>
#include <iostream>
#include <fstream>
#include <sstream>
# include <cstdlib>
# include <ctime>
# include <iomanip>


using namespace std;


// This program splits tab file in a set of N files. 
// First N-1 files are balanced and will contain same number of sequences,
//  while the last one may be slighlt larger or smaller
// first column of the file is used as a coherent index.
// note that if the same index appears multiple times SUBSEQUENTLY
// the series of continuous lines with the same index will NOT be broken.
//  
// USAGE:  ./a.out  infile    output_filename_root    Nfiles


// SPDX-License-Identifier: CC-BY-4.0

int main(int argc, char** argv) {

	ifstream infile; // input 
	ofstream outfile; // output

    // filenames
	string infile_path, output_filename_root, outfile_path;
	stringstream sstm;
	// input file
	sstm << argv[1];
	infile_path = sstm.str();
	// output file
	sstm.str("");
	sstm << argv[2];
	output_filename_root = sstm.str();


	int Nfiles;
	Nfiles=strtol(argv[3], NULL, 10);

 	int nfile=1;
	long long int nquery = 0;
	long long int tot_query_proteins = 0;
	long long int queries_per_file = 0;
	long long int lastq = 0;
    string line; 

	vector<long int> qIDs;

	cout << "\nInput File: " << infile_path << endl;
	cout << "Output Filename Root: " << output_filename_root << endl;
	cout << "Nfiles: " << Nfiles << endl;

	// FIRST read the file to get the number of all queries

	infile.open (infile_path.c_str());
	if (!infile.is_open()) { cout << "INFILE" << infile_path << " NOT GOOD" << endl; return 1; }
	long int oldtemp=-1;
	while (getline(infile, line)) {
		long int temp;
		istringstream iss(line); // make fileline into stream
		// leggi dallo stream (controllando IDTYPE)
		iss >> temp;
		if (temp != oldtemp) {
			oldtemp = temp;
			qIDs.push_back(temp);
		}
	}
	infile.close();

	tot_query_proteins = qIDs.size();

	if (Nfiles > 3) {
		queries_per_file = (tot_query_proteins / Nfiles) + (tot_query_proteins % Nfiles) / (Nfiles - 1) + (tot_query_proteins % (Nfiles - 1)) / (Nfiles - 2) + (tot_query_proteins % (Nfiles - 2)) / (Nfiles - 3);
	}
	else if (Nfiles > 2) { queries_per_file = (tot_query_proteins / Nfiles) + (tot_query_proteins % Nfiles) / (Nfiles - 1) + (tot_query_proteins % (Nfiles - 1)) / (Nfiles - 2); }
	else { queries_per_file = (tot_query_proteins / Nfiles) + (tot_query_proteins % Nfiles) / (Nfiles - 1); }

	cout << "We have tot_query_proteins=" << tot_query_proteins << endl;
	cout << "We divide them in " << Nfiles << " files" << endl;
	cout << "First N-1 file will contain " << queries_per_file << " queries " << endl;
	cout << "The last file will contain " << tot_query_proteins - queries_per_file * (Nfiles - 1) << endl;

    // ++++++++++++++++++++++++++++++++++ READ FROM FILE  // WRITE FILES +++++++++++++++++++++++++++++++++++++++++++++++++++
 	
	infile.open(infile_path.c_str());
	if (!infile.is_open()) { cout << "INFILE" << infile_path << " NOT GOOD" << endl; return 1; }
	
 	//open the first file to be written
 	//first, define filename:
 	sstm.str("");
    sstm << output_filename_root << "." << nfile;
	outfile_path = sstm.str();

 	outfile.open (outfile_path.c_str(),ios::app); 
	if (!outfile.is_open()) { cout << "OUTFILE " << outfile_path << " NOT GOOD" << endl; return 1; }

	//set firstq and lastq
	cout << queries_per_file << " queries x file" << endl;
	cout << qIDs[0] << " " << qIDs[1] << " " << qIDs[tot_query_proteins - 1] << " " << qIDs[queries_per_file] << endl;
	lastq = qIDs[queries_per_file];

	cout << "change when query is " << lastq << endl;
	while( getline(infile, line)) {
		long int temp;
		istringstream iss(line); // make fileline into stream
		// leggi dallo stream (controllando IDTYPE)
		iss >> temp;
		//if(nline%checklines==0) cout << "line " << nline << " of nfile " << nfile << " Nlines " << Nlines << " nfile*Nlines " << nfile*Nlines << endl;
		if (temp!=lastq) {
			//write
			outfile << line << "\n";
		}
		else {
			// last file? keep writing
			if (nfile == Nfiles) {
				// write
				outfile << line << "\n";
			}
			else {
				outfile.close();
				nfile++;
				cout << "!!! Opening file " << nfile << endl;

				lastq = qIDs[queries_per_file * nfile];

				//open the new file to be written
				//define filename:
				sstm.str("");
				sstm << output_filename_root << "." << nfile;
				outfile_path = sstm.str();

				outfile.open(outfile_path.c_str(), ios::app);
				if (!outfile.is_open()) { cout << "OUTFILE " << outfile_path << " NOT GOOD" << endl; return 1; }
				outfile << line << "\n";
			}
		}
	}
	
	
	infile.close();
	outfile.close();




   return 0;


}



