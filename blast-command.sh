
#!/bin/bash

# Author: Elena Tea Russo @ Institute for Research and Technologies (Area Science Park) & SISSA. 
# Package: DPCfamW version 1.0
# Description: script to run blast search. 

# SPDX-License-Identifier: CC-BY-4.0

export LC_NUMERIC="en_US.UTF-8"

folder=$1
big_in_fasta_wp=$2
fastas_folder=$3
out_blasted_folder_wp=$4
Nblocks=$5

# check if makeblastdb & blastp are installed

if ! command -v makeblastdb &> /dev/null
then
    echo "makeblastdb could not be found"
    exit
else
    echo "OK, makeblastdb found"
fi

if ! command -v blastp &> /dev/null
then
    echo "blastp could not be found"
    exit
else
    echo "OK, blastp found"
fi

# check if Nblocks variable is reasonable
if [ -z $Nblocks ]
then
 echo "\$Nblocks is empty! ABORTING. Check your configure file!"
 exit 1
fi

[ -n "$Nblocks" ] && [ "$Nblocks" -eq "$Nblocks" ] 2>/dev/null
if [ $? -ne 0 ]; then
   echo "$Nblocks is not an integer number! Check your configure file!"
   exit 1
fi

if [ 1 -gt $Nblocks ]; then
 echo "$Nblocks is not a valid number of blocks! Check your configure file!"
 exit 1
fi


# create blast databse (using global file) 
echo "Runnig makeblastdb"
makeblastdb -in ${big_in_fasta_wp} -parse_seqids -out ${folder}"/blastdb" -dbtype prot             
# run blast search(es)
echo "Runnig blastp"

for ((i=1;i<=Nblocks;++i)); do
time blastp -query ${fastas_folder}/${i}".fasta" -db ${folder}"/blastdb" -evalue 0.1   -max_target_seqs 20000 -out ${out_blasted_folder_wp}/${i}".blasted" -outfmt "6 qseqid sseqid qstart qend sstart send qlen slen length pident evalue score" -num_threads 4 ; done
