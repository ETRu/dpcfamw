#!/bin/bash

# Author: Elena Tea Russo @ Institute for Research and Technologies (Area Science Park) & SISSA. 
# Package: DPCfamW version 1.0
# Description: wrapper for input2binary.x to convert cleaned primary clustering output into binary

# SPDX-License-Identifier: CC-BY-4.0


export LC_NUMERIC="en_US.UTF-8"

input_folder=$1
output_folder=$2
Nblocks=$3


# check if Nblocks variable is reasonable
if [ -z $Nblocks ]
then
 echo "\$Nblocks is empty! ABORTING. Check your configure file!"
 exit 1
fi

[ -n "$Nblocks" ] && [ "$Nblocks" -eq "$Nblocks" ] 2>/dev/null
if [ $? -ne 0 ]; then
   echo "$Nblocks is not an integer number! Check your configure file!"
   exit 1
fi

if [ 1 -gt $Nblocks ]; then
 echo "$Nblocks is not a valid number of blocks! Check your configure file!"
 exit 1
fi


# cycle over Nblocks 

for ((i=1;i<=Nblocks;++i)); do
time bin/input2binary.x ${input_folder}/C${i}.cl -o ${output_folder}/${i}.bin
done